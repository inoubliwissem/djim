import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import org.apache.spark.broadcast.Broadcast;
import scala.Array;
import scala.Tuple2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

public class Main {

    public static void main(String args[]) {

        // remove old data
        ProcessBuilder processBuilder = new ProcessBuilder();
        try {
            processBuilder.command("//home//hadoop//_10//clean.sh");
            Process process = processBuilder.start();
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

        long start=System.currentTimeMillis();

        JavaSparkContext sc = new JavaSparkContext("local", "DJIM");
        final Broadcast<Integer> support = sc.broadcast(5);
        final Broadcast<Double> minconf = sc.broadcast(0.9);
        // read the dataset
        JavaRDD<String> lines = sc.textFile("f2");
        // share all the distinct candidats with all machines
        List<String> transactionsList = lines.collect();
        final Broadcast<List<String>> sharedtransaction = sc.broadcast(transactionsList);
        // get all transactions
        JavaPairRDD<String, String> trans = lines.mapToPair(new PairFunction<String, String, String>() {
            public Tuple2<String, String> call(String s) throws Exception {
                String parts[] = s.split(":");
                return new Tuple2<String, String>(parts[0], parts[1]);
            }
        });
        // get all items
        JavaRDD<Tuple2<String, String>> items = trans.flatMap(new FlatMapFunction<Tuple2<String, String>, Tuple2<String, String>>() {
            public Iterator<Tuple2<String, String>> call(Tuple2<String, String> stringStringTuple2) throws Exception {
                String transID = stringStringTuple2._1;
                ArrayList<Tuple2<String, String>> item = new ArrayList<Tuple2<String, String>>();
                for (String elm : stringStringTuple2._2.split(",")) {
                    item.add(new Tuple2<String, String>(elm, transID));
                }
                return item.iterator();
            }
        });
        JavaPairRDD<String, String> itemTrans = items.mapToPair(new PairFunction<Tuple2<String, String>, String, String>() {
            public Tuple2<String, String> call(Tuple2<String, String> stringStringTuple2) throws Exception {
                return new Tuple2<String, String>(stringStringTuple2._1, stringStringTuple2._2);
            }
        });
        // contains for each item a set of transaction ids B [1] A [1,5]
        JavaPairRDD<String, Iterable<String>> distinctItem = itemTrans.groupByKey();
        // print all items
        distinctItem.foreach(new VoidFunction<Tuple2<String, Iterable<String>>>() {
            public void call(Tuple2<String, Iterable<String>> stringIterableTuple2) throws Exception {
                System.out.println(stringIterableTuple2._1 + ":" + stringIterableTuple2._2);
            }
        });
        // get all distinct items
        JavaRDD<String> AlldistinctItems = distinctItem.keys();
        // filter the items that have minus number of transactions
        JavaPairRDD<String, Iterable<String>> frequentdistinctItem = distinctItem.filter(new Function<Tuple2<String, Iterable<String>>, Boolean>() {
            public Boolean call(Tuple2<String, Iterable<String>> stringIterableTuple2) throws Exception {
                Iterator it = stringIterableTuple2._2.iterator();
                int nb = 0;
                while (it.hasNext()) {
                    it.next();
                    nb++;
                }
                return nb > support.getValue();
            }
        });
        JavaRDD<String> distinctItems = distinctItem.keys();
        // write intermediate results
        final JavaRDD<String> tid = frequentdistinctItem.map(new Function<Tuple2<String, Iterable<String>>, String>() {
            public String call(Tuple2<String, Iterable<String>> stringIterableTuple2) throws Exception {
                String rst = "";
                rst += stringIterableTuple2._1 + ":";
                Iterator it = stringIterableTuple2._2.iterator();
                while (it.hasNext()) {
                    rst += it.next().toString() + ",";
                }
                rst = rst.substring(0, rst.length() - 1);
                return rst;
            }
        });
        tid.saveAsTextFile("TID//tid1");
        tid.saveAsTextFile("ExtraID//extratid1");
        tid.saveAsTextFile("SD//sd1");
        AlldistinctItems.saveAsTextFile("candidatof1");
        // the end of the first step or first level


        final Broadcast<List<String>> itemF = sc.broadcast(AlldistinctItems.collect());
        // for candidats which have two items
        int i = 2;
        boolean again = true;
        if(frequentdistinctItem.count()>0)
           again=false;
        while (i < 4000 && again) {
            // share the size of current candidat
            final Broadcast<Integer> taille = sc.broadcast(i);

            // read n-1 candidat to generate the next candidat
            JavaRDD<String> liness = sc.textFile("candidatof" + (i - 1) + "/part-00000").map(new Function<String, String>() {
                public String call(String s) throws Exception {
                    return s;
                }
            });

            JavaRDD<String> candidat = liness.flatMap(new FlatMapFunction<String, String>() {
                public Iterator<String> call(String s) throws Exception {
                    ArrayList<String> candi = new ArrayList<String>();
                    for (String elm : itemF.getValue()) {
                        if (!s.contains(elm)) {
                            candi.add(s + elm);
                        }
                    }
                    return candi.iterator();
                }
            });
            JavaPairRDD<String, String> tids = candidat.mapToPair(new PairFunction<String, String, String>() {
                public Tuple2<String, String> call(String s) throws Exception {

                    String part1 = s.substring(0, (taille.getValue() - 1));
                    String part2 = s.substring((taille.getValue() - 1));
                    File file = new File("TID//tid" + (taille.getValue() - 1) + "//part-00000");
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    if (part1.length() > 1) {
                        part2 = part1.substring(0, (part1.length() - 1)).concat(part2);
                    }
                    String st;
                    Set<Integer> tidP1 = new HashSet<Integer>();
                    Set<Integer> tidP2 = new HashSet<Integer>();

                    while ((st = br.readLine()) != null) {
                        String[] parts = st.split(":");
                        if (parts[0].equals(part1)) {
                            String[] idTrans = parts[1].split(",");
                            for (String elm : idTrans) {
                                tidP1.add(new Integer(elm));
                            }

                        } else if (parts[0].equals(part2)) {
                            String[] idTrans = parts[1].split(",");
                            for (String elm : idTrans) {
                                tidP2.add(new Integer(elm));
                            }
                        }
                    }

                    br.close();
                    System.out.println("Cand is " + s + " on  " + taille.getValue() + " p1 (" + part1 + ") are " + tidP1 + " and p2 (" + part2 + ") are " + tidP2);
                    tidP1.retainAll(tidP2);
                    Iterator<Integer> translist = tidP1.iterator();
                    String trans = "";
                    if (tidP1.size() > 0) {
                        for (Integer elm : tidP1) {
                            trans += elm + ",";
                        }
                        trans = trans.substring(0, trans.length() - 1);
                    }
                    return new Tuple2<String, String>(s, trans);
                }
            });
            JavaPairRDD<String, String> filtredCandidat = tids.filter(new Function<Tuple2<String, String>, Boolean>() {
                public Boolean call(Tuple2<String, String> stringStringTuple2) throws Exception {
                    return stringStringTuple2._2.split(",").length >= support.getValue();
                }
            });
          //  System.out.println("TID ************************************************************"+filtredCandidat.count());
            filtredCandidat.saveAsTextFile("TID//tid" + taille.getValue());
            // if we do not have any new candidat we stop this iterative bloc

            // new ExtratID
            JavaPairRDD<String, String> extrta = candidat.mapToPair(new PairFunction<String, String, String>() {
                public Tuple2<String, String> call(String s) throws Exception {
                    String part1 = s.substring(0, (taille.getValue() - 1));
                    String part2 = s.substring((taille.getValue() - 1));
                    List<String> allTrans = sharedtransaction.getValue();
                    List<Integer> nbP2 = new ArrayList<Integer>();
                    // check all part1
                    List<String> items = new ArrayList<String>();
                    items = Arrays.asList(part1.split(""));
                    // all transaction contains P2
                    for (String t : allTrans) {
                        String[] parts = t.split(":");
                        //   String []idTrans=parts[1].split(",");
                        List<String> Titems = Arrays.asList(parts[1].split(","));
                        if (Titems.contains(part2)) {
                            nbP2.add(new Integer(parts[0]));
                        }
                        for (String it : items) {
                            if (Titems.contains(it)) {
                                nbP2.remove(new Integer(parts[0]));
                            }
                        }
                    }
                    String rst = "";
                    for (Integer i : nbP2) {
                        rst += i + ",";
                    }
                    if (rst.length() > 0) {
                        rst = rst.substring(0, rst.length() - 1);
                    }
                    return new Tuple2<String, String>(s, rst);
                }
            });

            // filter the ExtratID list
            JavaPairRDD<String, String> filtredCandidatEx = extrta.filter(new Function<Tuple2<String, String>, Boolean>() {
                public Boolean call(Tuple2<String, String> stringStringTuple2) throws Exception {
                    return stringStringTuple2._2.split(",").length >= support.getValue();
                }
            });
            //write the ExtratID list
            filtredCandidatEx.saveAsTextFile("ExtraID//extratid" + i);
            //write the new cnadidates with size of i
            candidat.saveAsTextFile("candidatof" + i);
            // get SD for each candidat
            JavaPairRDD<String, String> sdCandidat = candidat.mapToPair(new PairFunction<String, String, String>() {
                public Tuple2<String, String> call(String s) throws Exception {
                    String part1 = s.substring(0, (taille.getValue() - 1));
                    String part2 = s.substring((taille.getValue() - 1));
                    String st = "";
                    boolean tv = false;
                    List<Integer> tidP1 = new ArrayList<Integer>();
                    List<Integer> tidP2 = new ArrayList<Integer>();
                    File file = new File("TID//tid" + (taille.getValue() - 1) + "//part-00000");
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    while ((st = br.readLine()) != null && !tv) {
                        String[] parts = st.split(":");
                        if (parts[0].equals(part1)) {
                            String[] idTrans = parts[1].split(",");
                            tv = true;
                            for (String elm : idTrans) {
                                tidP1.add(new Integer(elm));
                            }
                        }
                    }
                    br.close();
                    tv = false;
                    File file2 = new File("ExtraID//extratid" + (taille.getValue() - 1) + "//part-00000");
                    BufferedReader br2 = new BufferedReader(new FileReader(file2));
                    while ((st = br2.readLine()) != null && !tv) {
                        String[] parts = st.split(":");
                        if (parts[0].equals(part2)) {
                            String[] idTrans = parts[1].split(",");
                            tv = true;
                            for (String elm : idTrans) {
                                tidP2.add(new Integer(elm));
                            }
                        }
                    }
                    br2.close();
                    return new Tuple2<String, String>(s, "" + (tidP1.size() + tidP2.size()));
                }
            });
            //filter the SD
            JavaPairRDD<String, String> filterdSDCandidat = sdCandidat.filter(new Function<Tuple2<String, String>, Boolean>() {
                public Boolean call(Tuple2<String, String> stringStringTuple2) throws Exception {
                    return Double.parseDouble(stringStringTuple2._2) > support.getValue();
                }
            });
            //write the sd for all filterd candiat
            filterdSDCandidat.saveAsTextFile("SD//sd" + i);

            // correlation
            JavaPairRDD<String, Double> CorrCandidat = candidat.mapToPair(new PairFunction<String, String, Double>() {
                public Tuple2<String, Double> call(String s) throws Exception {
                    boolean tv = false;
                    String st = "";
                    List<Integer> tidP1 = new ArrayList<Integer>();
                    List<Integer> tidP2 = new ArrayList<Integer>();

                    File file = new File("TID//tid" + (taille.getValue() - 1) + "//part-00000");
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    while ((st = br.readLine()) != null && !tv) {
                        String[] parts = st.split(":");
                        if (parts[0].equals(s)) {
                            String[] idTrans = parts[1].split(",");
                            tv = true;
                            for (String elm : idTrans) {
                                tidP1.add(new Integer(elm));
                            }
                        }
                    }
                    br.close();
                    tv = false;
                    File file2 = new File("ExtraID//extratid" + (taille.getValue() - 1) + "//part-00000");
                    BufferedReader br2 = new BufferedReader(new FileReader(file2));
                    while ((st = br2.readLine()) != null && !tv) {
                        String[] parts = st.split(":");
                        if (parts[0].equals(s)) {
                            String[] idTrans = parts[1].split(",");
                            tv = true;
                            for (String elm : idTrans) {
                                tidP2.add(new Integer(elm));
                            }
                        }
                    }
                    br2.close();
                    Double correleation = new Double(0);
                    if (tidP2.size() > 0)
                        correleation = new Double(tidP1.size()) / new Double(tidP2.size());

                    return new Tuple2<String, Double>(s, correleation);
                }
            });
            JavaPairRDD<String, Double> FiltredCorrCandidat = CorrCandidat.filter(new Function<Tuple2<String, Double>, Boolean>() {
                public Boolean call(Tuple2<String, Double> stringDoubleTuple2) throws Exception {
                    return stringDoubleTuple2._2 > minconf.getValue();
                }
            });
            FiltredCorrCandidat.saveAsTextFile("Correlation//corr" + i);
            i++;
            if(filterdSDCandidat.count()==0 || filtredCandidatEx.count()==0 || filterdSDCandidat.count()==0 || filtredCandidat.count()==0){
                again=false;
                System.out.println("********************************************************** stop on "+taille.getValue());
            }
        }
        //agregate all candidates in one list
        System.out.print("the number of candidat size is " + i);
        List<String> files = new ArrayList<String>();
        String pathcandidat = "";
        String pathTID = "";
        String pathExtraID = "";
        String pathSD = "";
        String pathCorrelation = "";
        for (int j = 1; j <= i; j++) {
            File tempFile = new File("candidatof" + j + "/part-00000");
            File tempFile2 = new File("TID/tid" + j + "/part-00000");
            File tempFile3 = new File("ExtraID/extraid" + j + "/part-00000");
            if (tempFile.exists()) {
                files.add("candidatof" + j + "/part-00000");
                pathcandidat += "candidatof" + j + "/part-00000" + ",";
            }

            if (tempFile2.exists()) {
                pathTID += "TID/tid" + j + "/part-00000" + ",";
            }

            if (tempFile3.exists()) {
                pathExtraID += "ExtraID/extraid" + j + "/part-00000" + ",";
            }





        }

        if (pathcandidat.length() > 1) {
            pathcandidat = pathcandidat.substring(0, pathcandidat.length() - 1);

            JavaRDD<String> candidats = sc.textFile(pathcandidat);
            candidats.repartition(1).saveAsTextFile("allcandidats");
        }

        if (pathTID.length() > 1) {
            pathTID = pathTID.substring(0, pathTID.length() - 1);

            JavaRDD<String> allTid = sc.textFile(pathTID);
            allTid.repartition(1).saveAsTextFile("allTID");
        }

        if (pathExtraID.length() > 1) {
            pathExtraID = pathExtraID.substring(0, pathExtraID.length() - 1);

            JavaRDD<String> allExtrat = sc.textFile(pathExtraID);
            allExtrat.repartition(1).saveAsTextFile("allEXTRAID");
        }

         System.out.print("Running time is "+(System.currentTimeMillis()-start));
    }
}
